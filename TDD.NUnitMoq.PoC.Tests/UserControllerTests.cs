﻿using System.Threading.Tasks;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using TDD.NUnitMoq.PoC.Models;

namespace TDD.NUnitMoq.PoC.Tests
{
    [TestFixture]
    public class UserControllerTests
    {
        private UserController _sut;
        private Mock<IUserService> _userServiceMock;

        [SetUp]
        public void Setup()
        {
            _userServiceMock = new Mock<IUserService>();
            _sut = new UserController(_userServiceMock.Object);
        }

        [Test]
        public void Get_ExistentUser_ReturnsOk()
        {
            //arrange
            var userId = 1;
            var userDto = new Mock<UserDto>();

            _userServiceMock.Setup(x => x.GetUserById(userId)).Returns(userDto.Object);

            //act
            var result = _sut.Get(userId);

            //assert
            Assert.IsTrue(result is OkNegotiatedContentResult<UserDto>);
            Assert.AreEqual(userDto.Object, (result as OkNegotiatedContentResult<UserDto>).Content);
        }

        [Test]
        public async Task GetAsync_ExistentUser_ReturnsOk()
        {
            //arrange
            var userId = 1;
            var userDto = new Mock<UserDto>();
            _userServiceMock.Setup(x => x.GetUserById(userId)).Returns(userDto.Object);

            //act
            var result = await _sut.GetAsync(userId);

            //assert
            Assert.IsTrue(result is OkNegotiatedContentResult<UserDto>);
            Assert.AreEqual(userDto.Object, (result as OkNegotiatedContentResult<UserDto>).Content);
            _userServiceMock.Verify(x=>x.GetUserById(userId));
        }

        [Test]
        public void Get_ExistentUser_ReturnsNotFound()
        {
            //arrange
            var userId = 1;

            _userServiceMock.Setup(x => x.GetUserById(userId)).Returns(()=>null);

            //act
            var result = _sut.Get(userId);

            //assert
            Assert.IsTrue(result is NotFoundResult);
            _userServiceMock.Verify(x=>x.GetUserById(userId));
        }

        [Test]
        public async Task GetAsync_ExistentUser_ReturnsNotFound()
        {
            //arrange
            var userId = 1;

            _userServiceMock.Setup(x => x.GetUserById(userId)).Returns(()=>null);

            //act
            var result = await _sut.GetAsync(userId);

            //assert
            Assert.IsTrue(result is NotFoundResult);
            _userServiceMock.Verify(x=>x.GetUserById(userId));
        }
    }
}
