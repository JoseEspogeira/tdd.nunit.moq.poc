﻿namespace TDD.NUnitMoq.PoC.Models
{
    public class UserDto
    {
        protected UserDto()
        { }

        public UserDto(int id, string firstName, string lastName, int age)
        {
            Id = id;
            FullName = $"{firstName} {lastName}";
            Age = $"{age} years old";
        }
        public int Id { get; }
        public string FullName { get; }
        public string Age { get; }
    }
}
