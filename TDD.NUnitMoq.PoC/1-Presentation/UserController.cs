﻿using System.Threading.Tasks;
using System.Web.Http;
using TDD.NUnitMoq.PoC.Models;

namespace TDD.NUnitMoq.PoC
{
    [RoutePrefix("api/users")]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            var result = _userService.GetUserById(id);

            return result == null ? (IHttpActionResult) NotFound() : Ok(result);
        }

        [HttpGet]
        [Route("async/{id}")]
        public async Task<IHttpActionResult> GetAsync(int id)
        {
            UserDto result = null;

            await Task.Run(() =>
            {
                result = _userService.GetUserById(id);
            });

            return result == null ? (IHttpActionResult) NotFound() : Ok(result);
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody]UserDto user)
        {
            var result = _userService.CreateUser(user);

            return result == null ? (IHttpActionResult) BadRequest("bahh") : Created("", result);
        }

        [HttpPost]
        [Route("async")]
        public async Task<IHttpActionResult> PostAsync([FromBody]UserDto user)
        {
            UserDto result = null;

            await Task.Run(() =>
            {
                result = _userService.CreateUser(user);
            });

            return result == null ? (IHttpActionResult) BadRequest("bahh") : Created("/", result);
        }
    }
}
