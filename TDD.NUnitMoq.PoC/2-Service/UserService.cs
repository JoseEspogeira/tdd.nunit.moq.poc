﻿using System.Collections.Generic;
using TDD.NUnitMoq.PoC.Models;

namespace TDD.NUnitMoq.PoC
{
    public class UserService : IUserService
    {
        private readonly UserRepository _userRepository = new UserRepository();
        public UserDto GetUserById(int id)
        {
            throw new System.NotImplementedException();
        }

        public IList<UserDto> GetAllUsers()
        {
            throw new System.NotImplementedException();
        }

        public UserDto CreateUser(UserDto user)
        {
            throw new System.NotImplementedException();
        }

        public UserDto UpdateUser()
        {
            throw new System.NotImplementedException();
        }

        public void DeleteUser(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
