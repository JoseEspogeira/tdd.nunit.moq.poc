﻿using System.Collections.Generic;
using TDD.NUnitMoq.PoC.Models;

namespace TDD.NUnitMoq.PoC
{
    public interface IUserService
    {
        UserDto GetUserById(int id);
        IList<UserDto> GetAllUsers();
        UserDto CreateUser(UserDto user);
        UserDto UpdateUser();
        void DeleteUser(int id);
    }
}
