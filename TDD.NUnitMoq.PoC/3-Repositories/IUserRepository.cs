﻿using TDD.NUnitMoq.PoC.Models;

namespace TDD.NUnitMoq.PoC
{
    public interface IUserRepository
    {
        User Get(int id);
        User Insert(User user);
        User Update(User user);
        void Delete(User user);

    }
}
